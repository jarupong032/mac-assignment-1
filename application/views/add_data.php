
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Create User Page</title>
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

	<style type="text/css">
	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 100px 400px 100px 400px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 50px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 50px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>


</head>
<body>


<table class="table table-borderless-sm" border="1">
    <thead class="thead-light">
      <tr>
        <th colspan="4" >User List Page</th>
      </tr>
    </thead>
    <tbody>
		<form action="/mac/assignment/save" method="post">
		<tr>
			<td align="center">
				<label for="firstname">Firstname</label>
			</td>
			<td align="center">
				<input type="text" class="form-control" id="user_firstname"  name="user_firstname" required>
			</td>
		</tr>

		<tr>
			<td align="center">
				<label for="lastname">Lastname</label>
			</td>
			<td align="center">
				<input type="text" class="form-control" id="user_lastname"  name="user_lastname" required>
			</td>
		</tr>

		<tr>
			<td align="center">
				<label for="email">E-mail</label>
			</td>
			<td align="center">
				<input type="text" class="form-control" id="user_email"  name="user_email" required>
			</td>
		</tr>

		<tr>
			<td align="center">
				<label for="phone">Phone</label>
			</td>
			<td>
				<input type="text" class="form-control" id="user_phone"  name="user_phone" required>
			</td>
		</tr>

		<tr>
			<td align="right" colspan="2">
				<button type="submit" class="btn btn-primary ">Save</button>
				<button type="button" class="btn" onclick="location.replace('/mac/assignment')">Cancel</button>
			</form>
			</td>
		</tr>
		
		
	</tbody>
</table>
				




</div>
</body>
</html>