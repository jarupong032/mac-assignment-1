<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Assignment1</title>
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>

	<style type="text/css">
	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>


</head>
<body>
<div class="container">
	<div class="card">
	<h4 class="card-header">User List Page </h4>
	<div class="card-body">
	<table class="table">
	  <thead>
	  <tr>
		  <th colspan="4"><button type="button" class="btn btn-primary btn-sm" onclick="location.replace('/mac/assignment/add')" >Create New User</button></th>
	  </tr>
	  <tr>
		  <th>firstname</th>
		  <th>lastname</th>
		  <th>email</th>
		  <th>phone</th>
	  </tr>
      </thead>
	  <tbody>
	  <?php foreach($ex_users as $user) { ?>
      <tr>
		<td><a href="/mac/assignment/edit/<?php echo $user->user_id ?>"><?php echo $user->user_firstname ?></a></td>
		<td><a href="/mac/assignment/edit/<?php echo $user->user_id ?>"><?php echo $user->user_lastname ?></a></td>
		<td><a href="/mac/assignment/edit/<?php echo $user->user_id ?>"><?php echo $user->user_email ?></a></td>
		<td><a href="/mac/assignment/edit/<?php echo $user->user_id ?>"><?php echo $user->user_phone ?></a></td>
      </tr>
	  <?php } ?>

    </tbody>
	</table>
	</div>
	</div>
</div>
</body>
</html>
