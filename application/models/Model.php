<?php

class Model extends CI_Model{

	public function listUser(){
		$result = $this->db
			->get('ex_users')
			->result();
		return $result;
	}

	public function getUserById($user_id){
		$ex_users = $this->db->where('user_id', $user_id)
			->get('ex_users')
			->result();
		return $ex_users[0];
	}

	public function saveuser($user_firstname,$user_lastname,$user_email,$user_phone) {
		$this->db
			->set('user_firstname', $user_firstname)
			->set('user_lastname', $user_lastname)
			->set('user_email', $user_email)
			->set('user_phone', $user_phone)
			->insert('ex_users');
		if($this->db->affected_rows()==0){
		return 0;
		}else{
		return $this->db->insert_id();
		}
	}

	public function updateUser($user_id,$user_firstname,$user_lastname,$user_email,$user_phone) {
		$this->db
			->set('user_firstname', $user_firstname)
			->set('user_lastname', $user_lastname)
			->set('user_email', $user_email)
			->set('user_phone', $user_phone)
			->where('user_id', $user_id)
			->update('ex_users');
		return $this->db->affected_rows();
	}

	public function deleteUser($user_id) {
		$this->db
			->where('user_id', $user_id)
			->delete('ex_users');
		return $this->db->affected_rows();
	}


}
?>
