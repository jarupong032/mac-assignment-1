<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class assignment extends CI_Controller {



	public function index()
	{
	$this->load->database();
	$this->load->model('Model');
	$ex_users = $this->Model->listUser();
	$data = array(
		'ex_users'      => $ex_users
		);
	$this->load->view('show_data',$data);
	}

	public function add()
	{
		$this->load->model('Model');
		$this->load->view('add_data');
	}

	public function edit($user_id) {
		$this->load->database();
		$this->load->model('Model');
		$ex_users = $this->Model->getUserById($user_id);
		$data = array(
			'ex_users'      => $ex_users
			);
		$this->load->view('edit_data',$data);
	}

	public function delete($user_id) {
		$this->load->database();
		$this->load->model('Model');
		$ex_users = $this->Model->getUserById($user_id);
		$data = array(
			'ex_users'      => $ex_users
			);
		$this->load->view('delete_data',$data);
	}

	public function save() {
		$user_firstname = $_POST['user_firstname'];
        $user_lastname = $_POST['user_lastname'];
        $user_email = $_POST['user_email'];
		$user_phone = $_POST['user_phone'];
		$this->load->database();
		$this->load->model('Model');
		$row_count = $this->Model->saveUser($user_firstname, $user_lastname, $user_email, $user_phone);
		if($row_count>0){
            header('Location:/mac/assignment/');
        }
	}

	public function saveedit() {
		$user_firstname = $_POST['user_firstname'];
		$user_lastname = $_POST['user_lastname'];
		$user_email = $_POST['user_email'];
		$user_phone = $_POST['user_phone'];
		$user_id = $_POST['user_id'];
		$this->load->database();
		$this->load->model('Model');
		$row_count = $this->Model->updateUser($user_id,$user_firstname, $user_lastname, $user_email, $user_phone);
		if($row_count>0){
			header('Location:/mac/assignment/');
		}
	}

	public function del() {
		$user_id = $_POST['user_id'];
		$this->load->database();
		$this->load->model('Model');
		$row_count = $this->Model->deleteUser($user_id);
		if($row_count>0){
			header('Location:/mac/assignment/');
		}
	}


}

?>