﻿# Assignment 1 
 ***GET POST UPDATE and DELETE [Codeigniter]***
## What is CodeIgniter

CodeIgniter is an Application Development Framework - a toolkit - for people
who build web sites using PHP. Its goal is to enable you to develop projects
much faster than you could if you were writing code from scratch, by providing
a rich set of libraries for commonly needed tasks, as well as a simple
interface and logical structure to access these libraries. CodeIgniter lets
you creatively focus on your project by minimizing the amount of code needed
for a given task.

## Features
 - GET table from database
 - POST to table from database
 - UPDATE to table from database
 - DELETE to table from database

## Requirements
 - [Visual Studio Code](https://code.visualstudio.com/)
 - [AppServ](https://www.appserv.org/th/)  : Apache + PHP + MySQL
 - [CodeIgniter](https://codeigniter.com/download) : 3.1.5 or greater

## Installation
 
 - Please see the [installation](https://codeigniter.com/user_guide/installation/index.html) section of the CodeIgniter User Guide.

## License
 - Internship 2018 @ Xeersoft

 

