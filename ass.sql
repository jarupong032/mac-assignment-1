-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 29, 2018 at 02:23 PM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ass`
--

-- --------------------------------------------------------

--
-- Table structure for table `ex_users`
--

CREATE TABLE `ex_users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_firstname` varchar(45) DEFAULT NULL,
  `user_lastname` varchar(45) DEFAULT NULL,
  `user_email` varchar(250) DEFAULT NULL,
  `user_phone` varchar(20) DEFAULT NULL,
  `user_active` tinyint(1) UNSIGNED DEFAULT '1',
  `user_modified` datetime DEFAULT NULL,
  `user_modified_by` int(10) UNSIGNED DEFAULT NULL,
  `user_created` datetime DEFAULT NULL,
  `user_created_by` int(10) UNSIGNED DEFAULT NULL,
  `user_deleted` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ex_users`
--

INSERT INTO `ex_users` (`user_id`, `username`, `password`, `user_firstname`, `user_lastname`, `user_email`, `user_phone`, `user_active`, `user_modified`, `user_modified_by`, `user_created`, `user_created_by`, `user_deleted`) VALUES
(1, 'admin', '12345678901234567890123456789012', 'Mark_edit', 'Otto_edit', 'mark@gmail.com', '0889998888', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(2, 'ball', '12345678901234567890123456789012', 'Jacob', 'Thornton', 'jacob@gmail.com', '0889998888', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0),
(3, 'ong', '12345678901234567890123456789012', 'Larry the Bird', NULL, 'larry_the_bird@gmail.com', '0889998888', 1, '0000-00-00 00:00:00', 0, '0000-00-00 00:00:00', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ex_users`
--
ALTER TABLE `ex_users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `u_name` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ex_users`
--
ALTER TABLE `ex_users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
